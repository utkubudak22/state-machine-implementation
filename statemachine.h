/*
    @author: Utku Budak
    @TUM-Kennung: ge72quf
    @date: 2021 November
    This is the header file that consists of the required functions headers for the State Machine Implementation.
*/

#include <stdio.h>
#include <stdint.h>

// These define the possible states of the state machine.
enum states{
    POWER_UP = 0,
    WAIT,
    EXECUTE_COMMAND
};

typedef enum states State_Type;

// The variable that holds the current state of the state machine.
static State_Type current_state;

// This is the initialization function that defines the start state.
void state_machine_init(void);

// The functions for the states: power up, wait function, execute command.
void power_up_function(void);
void wait_function(void);
void execute_command_function(void);

static void (*state_table[])(void) = {power_up_function,
                                      wait_function,
                                      execute_command_function
};
