/*
    @author: Utku Budak
    @TUM-Kennung: ge72quf
    @date: 2021 November
    This is the source file that consists of the required functions for the State Machine Implementation.
*/

#include "statemachine.h"

void state_machine_init(void){
    current_state = POWER_UP;
}

void power_up_function(void){
    current_state = WAIT;
    printf("This is the function for the POWER UP state.\n");
}

void wait_function(void){
    current_state = EXECUTE_COMMAND;
    printf("This is the function for the WAIT state.\n");
}

void execute_command_function(void){
    printf("This is the function for the EXECUTE COMMAND state.\n");
}


int main(){

    state_machine_init();

    for(uint8_t i = 0; i < 3; i++){
        state_table[current_state]();
    }

    return 0;
}